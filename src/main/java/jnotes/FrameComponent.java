package jnotes;

import javax.swing.*;
import java.awt.*;

public class FrameComponent extends JFrame {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    FrameComponent(final PanelComponent panel) {
        init(panel);
    }

    void init(final PanelComponent panel) {
        final PanelComponent panelComponent = panel;
        setTitle(Constants.APP_TITLE);
        setMinimumSize(new Dimension(Constants.MAX_WIDTH, Constants.MAX_HEIGHT));
        setResizable(false);
        add(panelComponent, BorderLayout.CENTER);
        pack();
        //setAlwaysOnTop(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }
}