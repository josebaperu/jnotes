package jnotes;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class Utils {
	
	private static Utils utils;
	private Utils() {};
	private StringBuilder sb = null;
	private BufferedReader br = null;
    private Map<String, Model> models = null;
    private String line = null;
    
	public static Utils getInstance() {
		return utils == null? new Utils():utils;
	} 

    public String generateRandomId() {
        return UUID.randomUUID().toString();
    }

    @SuppressWarnings("finally")
    public  Map<String, Model> readJsonFile() {
        try {
            br = new BufferedReader(new FileReader(Constants.JSON));
            sb = new StringBuilder();
            line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            line = null;
            
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } finally {
            try {
                br.close();
                br = null;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (sb.toString() == null || sb.toString().length() == 0) {
                return new LinkedHashMap<String, Model>();
            } else {
                models = new LinkedHashMap<String, Model>();
            	final JSONArray jsonArray = new JSONArray(sb.toString());
                jsonArray.forEach(json -> {
                    Model model = new Model((JSONObject)json);
                    models.put(model.getId(), model);                	
                });
                return models;
            }
        }
    }

    public void writeJsonFile(final Map<String, Model> models) {
    	final JSONArray jsonArray = new JSONArray();
        models.entrySet().forEach(entry -> {
            jsonArray.put(entry.getValue().toJson());
        	
        });
        
        PrintWriter writer;
        try {
            writer = new PrintWriter(Constants.JSON, "UTF-8");
            writer.println(jsonArray.toString());
            writer.close();
            writer = null;
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
