package jnotes;

import mdlaf.MaterialLookAndFeel;

import javax.swing.*;
import java.io.File;

public class MainClass {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            try {
                File f = new File(Constants.JSON);
                if (!f.exists()) {
                	f.createNewFile();
                }
            	f = null;
            	final MaterialLookAndFeel materialLookAndFeel =new MaterialLookAndFeel();
            	UIManager.setLookAndFeel(materialLookAndFeel);
            	final PanelComponent panelComponent = new PanelComponent();
                final FrameComponent frame = new FrameComponent(panelComponent);
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
