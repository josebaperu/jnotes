package jnotes;

import org.json.JSONObject;

public class Model {

    private String id;
    private String title;
    private String text;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public JSONObject toJson() {
        final JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("title", title);
        json.put("text", text);
        return json;
    }

    public Model(JSONObject json) {
        setId(json.getString("id"));
        setText(json.getString("text"));
        setTitle(json.getString("title"));
    }

    public Model() {
    }

}
