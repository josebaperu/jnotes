package jnotes;

public class Constants {
    final public static int MAX_WIDTH = 800;
    final public static int MAX_HEIGHT = 600;
    final public static String NEW = "NEW";
    final public static String EDIT = "EDIT";
    final public static String NEXT = ">>";
    final public static String PREVIOUS = "<<";
    final public static String DELETE = "DELETE";
    final public static String SAVE = "SAVE";
    final public static String JSON = "data.json";
    final public static String DEFAULT_TITLE = "New Title";
    final public static String DEFAULT_TEXT = "New Text";
    final public static String APP_TITLE = "Swing TextEditor";
}