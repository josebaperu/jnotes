package jnotes;

import mdlaf.animation.MaterialUIMovement;
import mdlaf.utils.MaterialColors;

import javax.swing.*;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PanelComponent extends JPanel {

    private static final long serialVersionUID = 1L;
    private JButton newButton;
    private JButton previousButton;
    private JButton editButton;
    private JButton nextButton;
    private JButton deleteButton;
    private JTextField textField;
    private JTextPane textPane;
    private JPanel topPanel;
    private JPanel middlePanel;
    private JPanel bottomPanel;
    private List<JButton> buttons;
    private List<JPanel> panels;
    private Controller controller;
    private JScrollPane scrollPane;

    PanelComponent() {
        setBackground(MaterialColors.TEAL_200);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        init();
    }

    private void init() {
        generateButtonList();
        generatePanelList();

        GroupLayout gLayout = new GroupLayout(this);
        setLayout(gLayout);
        ParallelGroup hGroup = gLayout.createParallelGroup();
        gLayout.setHorizontalGroup(hGroup);
        SequentialGroup vGroup = gLayout.createSequentialGroup();
        gLayout.setVerticalGroup(vGroup);
        textPane = new JTextPane();
        textPane.setPreferredSize(new Dimension(Constants.MAX_WIDTH, 200));
        textPane.setForeground(Color.GRAY);
        textField = new JTextField("");
        textField.setEnabled(false);
        textPane.setEditable(false);

        textField.setPreferredSize(new Dimension(Constants.MAX_WIDTH, 30));
        textField.setBackground(MaterialColors.GRAY_100);
        textField.setForeground(Color.GRAY);
        textField.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
                if (textField.getText().equals(Constants.DEFAULT_TITLE)) {
                    textField.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                // TODO Auto-generated method stub

            }
        });

        textPane.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
                if (textPane.getText().equals(Constants.DEFAULT_TEXT)) {
                    textPane.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                // TODO Auto-generated method stub

            }
        });
        panels.forEach(panel -> {
            hGroup.addComponent(panel);
            vGroup.addComponent(panel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE);
            vGroup.addGap(10); }
        );

        middlePanel.add(textField);
        textPane.setBackground(MaterialColors.WHITE);
        bottomPanel.setLayout(new BorderLayout());
        scrollPane = new JScrollPane(textPane);
        scrollPane.getVerticalScrollBar().setUnitIncrement(5);
        scrollPane.setPreferredSize(new Dimension(Constants.MAX_WIDTH, 500));
        bottomPanel.add(scrollPane, BorderLayout.CENTER);
        controller = new Controller(textPane, textField, buttons);

        buttons.forEach(btn -> {
            btn.setMaximumSize(new Dimension(200, 200));
            btn.setBackground(MaterialColors.TEAL_200);
            btn.setForeground(MaterialColors.WHITE);
            MaterialUIMovement.add(btn, MaterialColors.BLUE_A200);
            btn.addActionListener(controller);
            topPanel.add(btn);
        });
        TextComponentContextActionsMenu.addTo(textPane);
        TextComponentContextActionsMenu.addTo(textField);
    }

    private void generateButtonList() {
        buttons = new LinkedList<JButton>();
        newButton = new JButton(Constants.NEW);
        previousButton = new JButton(Constants.PREVIOUS);
        editButton = new JButton(Constants.EDIT);
        nextButton = new JButton(Constants.NEXT);
        deleteButton = new JButton(Constants.DELETE);
        buttons.add(newButton);
        buttons.add(previousButton);
        buttons.add(editButton);
        buttons.add(nextButton);
        buttons.add(deleteButton);
    }

    private void generatePanelList() {
        panels = new LinkedList<JPanel>();
        topPanel = new JPanel();
        middlePanel = new JPanel();
        bottomPanel = new JPanel();
        panels.add(topPanel);
        panels.add(middlePanel);
        panels.add(bottomPanel);
    }
}
