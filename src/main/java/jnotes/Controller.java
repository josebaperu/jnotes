package jnotes;

import javax.swing.*;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class Controller implements ActionListener {

	private final JTextPane textPane;
	private final JTextField textField;
	private final List<JButton> buttons;
	private JButton button;
	private Map<String, Model> models;
	private Model current = null;
	private final Utils utils = Utils.getInstance();
	private List<String>  modelList;

	Controller(final JTextPane tp, final JTextField tf, final List<JButton> bts) {
		this.textPane = tp;
		this.textField = tf;
		this.buttons = bts;

		try {
			refreshModels();
			boolean isNotEmptyModel = models.entrySet().stream().findAny().isPresent();
			if (isNotEmptyModel) {
				getButton(Constants.EDIT).setEnabled(true);
				current = models.get(models.keySet().iterator().next());
				textPane.setText(current.getText());
				textField.setText(current.getTitle());
				getButton(Constants.PREVIOUS).setEnabled(false);
				getButton(Constants.NEXT).setEnabled(models.size() != 1);

			} else {
				getButton(Constants.EDIT).setEnabled(false);
				getButton(Constants.DELETE).setEnabled(false);
				getButton(Constants.PREVIOUS).setEnabled(false);
				getButton(Constants.NEXT).setEnabled(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setEditable(final boolean b) {
		textPane.setEditable(b);
		textField.setEnabled(b);
		Runtime.getRuntime().gc();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
        try {
            EventQueue.invokeLater(new Runnable() {
              @Override
              public void run() {
          		button = getButton(e.getActionCommand());
        		getButton(Constants.NEXT).setEnabled(true);
        		getButton(Constants.PREVIOUS).setEnabled(true);
        		switch (e.getActionCommand()) {
        		case Constants.DELETE:
        			if (models.size() > 0)
        				performDelete();
        			break;
        		case Constants.NEW:
        			setEditable(true);
        			performNew();
        			break;
        		case Constants.EDIT:
        			setEditable(true);
        			performEdit();
        			break;
        		case Constants.SAVE:
        			setEditable(false);
        			performSave();
        			break;
        		case Constants.NEXT:
        			performNext();
        			break;
        		case Constants.PREVIOUS:
        			performPrevious();
        			break;

        		}
        		textPane.setCaretPosition(0);
        		if (models != null) {
        			modelList = new ArrayList<String>(models.keySet());

        			if (models.size() == 0 || models.size() == 1) {
        				getButton(Constants.NEXT).setEnabled(false);
        				getButton(Constants.PREVIOUS).setEnabled(false);
        			} else if (modelList.get(0).equals(current.getId())) {
        				getButton(Constants.NEXT).setEnabled(true);
        				getButton(Constants.PREVIOUS).setEnabled(false);
        			} else if (modelList.get(modelList.size() - 1).equals(current.getId())) {
        				getButton(Constants.NEXT).setEnabled(false);
        				getButton(Constants.PREVIOUS).setEnabled(true);
        			}
        			modelList = null;
        		}
            	  
            	  
              }
            });
            Runtime.getRuntime().gc();
          } catch (Exception ex) {
            ex.printStackTrace();
          }
	}

	private void performSave() {
		current.setTitle(textField.getText());
		current.setText(textPane.getText());
		getButton(Constants.SAVE).setText(Constants.EDIT);
		getButton(Constants.DELETE).setEnabled(true);

		if (models == null || models.size() == 0) {
			models = new LinkedHashMap<String, Model>();
		}
		models.put(current.getId(), current);
		utils.writeJsonFile(models);
		Runtime.getRuntime().gc();
	}

	private void performEdit() {
		getButton(Constants.EDIT).setText(Constants.SAVE);
		Runtime.getRuntime().gc();
	}

	private void performDelete() {
		textPane.setEditable(false);
		textField.setEnabled(false);
		models.remove(current.getId());
		utils.writeJsonFile(models);
		if (models.size() > 0) {
			getButton(Constants.DELETE).setEnabled(true);
			current = models.get(models.keySet().iterator().next());
			textPane.setText(current.getText());
			textField.setText(current.getTitle());
		} else {
			textPane.setText("");
			textField.setText("");
			getButton(Constants.DELETE).setEnabled(false);

			if (getButton(Constants.EDIT) != null)
				getButton(Constants.EDIT).setEnabled(false);
			else
				getButton(Constants.SAVE).setEnabled(false);
		}
		Runtime.getRuntime().gc();
	}

	private void performNew() {
		if (getButton(Constants.SAVE) != null)
			getButton(Constants.SAVE).setEnabled(true);
		else {
			button = getButton(Constants.EDIT);
			button.setEnabled(true);
			button.setText(Constants.SAVE);
		}
		current = new Model();
		current.setId(utils.generateRandomId());
		textPane.setText(Constants.DEFAULT_TEXT);
		textField.setText(Constants.DEFAULT_TITLE);
		Runtime.getRuntime().gc();
	}

	private void performNext() {
		textPane.setEditable(false);
		textField.setEnabled(false);
		if (models.size() > 0) {
			getNext();
			textPane.setText(current.getText());
			textField.setText(current.getTitle());
			if (getButton(Constants.SAVE) != null)
				getButton(Constants.SAVE).setText(Constants.EDIT);
		} else {
			textPane.setText("");
			textField.setText("");
		}
		Runtime.getRuntime().gc();
	}

	private void getNext() {
		modelList = new ArrayList<String>(models.keySet());

		Integer index = null;
		for (int i = 0; i < modelList.size(); i++) {
			if (modelList.get(i).equals(current.getId()) && i < modelList.size() - 1) {
				index = i;

			}
		}
		if (index != null) {
			current = models.get(modelList.get(index + 1));
			textPane.setText(current.getText());
			textField.setText(current.getTitle());

		}
		modelList = null;
		Runtime.getRuntime().gc();
	}

	private void getPrevious() {
		modelList = new ArrayList<String>(models.keySet());
		Integer index = null;

		for (int i = 0; i < modelList.size(); i++) {
			if (modelList.get(i).equals(current.getId()) && i > 0) {
				index = i;
			}
		}
		if (index != null) {
			current = models.get(modelList.get(index - 1));
			textPane.setText(current.getText());
			textField.setText(current.getTitle());
		}
		modelList = null;
		Runtime.getRuntime().gc();
	}

	private void performPrevious() {
		textPane.setEditable(false);
		textField.setEnabled(false);
		if (models.size() > 0) {
			getPrevious();
			textPane.setText(current.getText());
			textField.setText(current.getTitle());

			if (getButton(Constants.SAVE) != null)
				getButton(Constants.SAVE).setText(Constants.EDIT);
		} else {
			textPane.setText("");
			textField.setText("");

		}
		Runtime.getRuntime().gc();
	}

	private JButton getButton(final String s) {
		return buttons.stream().filter(bt -> bt.getText().equals(s)).findAny().orElse(null);
	}

	private void refreshModels() {
		try {
			models = utils.readJsonFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}